/**
 * @module          Meta-Index
 * @author          cms-lab
 * @copyright       2018-2023 cms-lab
 * @link            https://cms-lab.com
 * @license         custom license: https://cms-lab.com/_documentation/meta_index/license.php
 * @license_terms   please see license
 *
 */

function set_up_delete (aRef, lang) {
	var msg= "";
	
	switch(lang) {
		case 'DE':
			msg= "Sind Sie sicher, das Sie das Element entfernen wollen?";
			break;
			
		case 'EN':
			msg= "Are you sure you want to delete the element?"
			break;
			
		default:
			msg= "Are you sure you want to delete the element?"
			break;
	}
	
	if (confirm( msg )) {
	
		var ref= aRef.form.job;
		if (ref) {
			ref.value="delete";
			aRef.form.submit();
		}
	}
}


