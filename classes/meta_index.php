<?php

/**
 * @module          Meta-Index
 * @author          cms-lab
 * @copyright       2018-2023 cms-lab
 * @link            https://cms-lab.com
 * @license         custom license: https://cms-lab.com/_documentation/meta_index/license.php
 * @license_terms   please see license
 *
 */

class meta_index extends LEPTON_abstract
{
	public array $pages = [];
	public array $modified_pages = [];
	public array $not_modified_pages = [];
	public string $addon_color = 'blue';
	public string $action_url = ADMIN_URL . '/admintools/tool.php?tool=meta_index';
	public string $action = LEPTON_URL . '/modules/meta_index/';	

	
	public object|null $oTwig = null;
	public LEPTON_database $database;
	public LEPTON_admin $admin;
	static $instance;

	public function initialize() 
	{
		$this->database = LEPTON_database::getInstance();
		$this->admin = LEPTON_admin::getInstance();		
		$this->oTwig = lib_twig_box::getInstance();
		$this->oTwig->registerModule('meta_index');		
		$this->init_tool();
	}
	
	public function init_tool( $sToolname = '' )
	{
		//get all pages
		$this->database->execute_query(
			"SELECT page_id, page_title, menu_title FROM ".TABLE_PREFIX."pages ORDER BY page_id " ,
			true,
			$this->pages,
			true
		);		

		//get all modified pages
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_meta_index ORDER BY page_id " ,
			true,
			$this->modified_pages,
			true
		);

		//get all not modified pages
		foreach( $this->pages as $display_page) {
			$found = false;
			foreach($this->modified_pages as $test_page) {
				if($test_page['page_id'] == $display_page['page_id'] ) 
				{
					$found = true;
					break;
				}
			}
			if($found === false) 
			{
				$this->not_modified_pages[] = $display_page;
			}
		}			
	}
	
	
	public function list_pages()
	{
		// delete obsolete entries
		foreach ($this->modified_pages as $existing) {
			if($this->database->get_one("SELECT page_id FROM ".TABLE_PREFIX."pages WHERE page_id = ".$existing['page_id']." ") == NULL) {
				$this->database->simple_query("DELETE FROM ".TABLE_PREFIX."mod_meta_index WHERE page_id = ".$existing['page_id']."");		
			}
		}	

		// data for twig template engine	
		$data = array(
			'oMIF'			=> $this,	
			'readme_link'	=> "https://cms-lab.com/_documentation/meta_index/readme.php",			
			'pages'			=> $this->pages,
			'leptoken'		=> get_leptoken(),			
			'modified_pages'=> $this->modified_pages
			);

		// get the template-engine		
		echo $this->oTwig->render( 
			"@meta_index/list.lte",	//	template-filename
			$data					//	template-data
		);
	
	}	

	public function show_info() 
	{
		// create links	
		$support_link = "<a href='#'>NO Live-Support / FAQ</a>";
		$readme_link = "<a href='https://cms-lab.com/_documentation/meta_index/readme.php' class='info' target='_blank'>Readme</a>";		

		// data for twig template engine	
		$data = array(
			'oMIF'			=> $this,
			'readme_link'	=> $readme_link,		
			'SUPPORT'		=> $support_link,		
			'image_url'		=> 'https://cms-lab.com/_documentation/media/meta_index/meta_index.jpg'
			);
			
		// get the template-engine		
		echo $this->oTwig->render( 
			"@meta_index/info.lte",	//	template-filename
			$data					//	template-data
		);		
		
	}
	
	function edit_link($id) 
	{
		if ( $id == 'new') 
		{
			// data for twig template engine	
			$data = array(	
				'oMIF'			=> $this,
				'action_url'	=> $this->action.'add_link.php',			
				'leptoken'		=> get_leptoken(),			
				'oMIF'			=> $this			
				);
				
			// get the template-engine		
			echo $this->oTwig->render( 
				"@meta_index/insert.lte",	//	template-filename
				$data						//	template-data
			);			
		}		

		if ( $id == 'edit') 
		{
			
			if(isset ($_POST['edit_link']) && is_numeric ($_POST['edit_link']) ) {
			$edit_id = $_POST['edit_link'];
			}

			$current_tag = $this->database->get_one("SELECT tag FROM ".TABLE_PREFIX."mod_meta_index WHERE id = ".$edit_id." ")	;	
			
			// data for twig template engine	
			$data = array(
				'oMIF'			=> $this,
				'addon_name'	=> 'meta_index',
				'addon_color'	=> $this->addon_color,			
				'action_url'	=>  $this->action.'save_link.php',
				'current_tag'	=> $current_tag,
				'current_id'	=> $edit_id,
				'leptoken'		=> get_leptoken()		
				);

			// get the template-engine
			echo $this->oTwig->render(
				"@meta_index/edit.lte",	//	template-filename
				$data					//	template-data
			);
		}
	}

	function delete_link() 
	{
		if(isset ($_POST['delete_link']) && is_numeric ($_POST['delete_link']) ) {
			$to_delete = $_POST['delete_link'];
		}		
		$this->database->simple_query("DELETE FROM ".TABLE_PREFIX."mod_meta_index WHERE id = '".$to_delete."'");		
		
		// Check if there is a db error, else success
		if($this->database->is_error()) {
			$this->admin->print_error($this->database->get_error());
		} else {
			$this->admin->print_success($this->language['record_deleted'], ADMIN_URL.'/admintools/tool.php?tool=meta_index');
		}

		// Print admin footer
		$this->admin->print_footer();	
	}	

} // end of class
