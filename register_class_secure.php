<?php

/**
 * @module          Meta-Index
 * @author          cms-lab
 * @copyright       2018-2023 cms-lab
 * @link            https://cms-lab.com
 * @license         custom license: https://cms-lab.com/_documentation/meta_index/license.php
 * @license_terms   please see license
 *
 */
 
$files_to_register = array(
	'add_link.php',
	'save_link.php'
);

LEPTON_secure::getInstance()->accessFiles( $files_to_register );

?>