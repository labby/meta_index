<?php

/**
 * @module          Meta-Index
 * @author          cms-lab
 * @copyright       2018-2023 cms-lab
 * @link            https://cms-lab.com
 * @license         custom license: https://cms-lab.com/_documentation/meta_index/license.php
 * @license_terms   please see license
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

//get instance of own module class
$oMIF = meta_index::getInstance();
$FORWARD = "Location: ".ADMIN_URL."/admintools/tool.php?tool=meta_index&leptoken=".get_leptoken();

if(isset($_POST['cancel']) ) {
	header( $FORWARD );
	exit();
}

// no id
if(!isset($_POST['save_link']) || ($_POST['save_link'] == '') ) {
	header( $FORWARD );
	exit();	
}


// save new meta_index link
if(isset ($_POST['save_link']) && is_numeric ($_POST['save_link']) ) {
	
	$database->simple_query("UPDATE ".TABLE_PREFIX."mod_meta_index SET tag = '".$_POST['tag']."' WHERE id = ".$_POST['save_link']."  ");			
	$oMIF->admin->print_success('record_ saved', ADMIN_URL."/admintools/tool.php?tool=meta_index");
}
	
