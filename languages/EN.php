<?php

/**
 * @module          Meta-Index
 * @author          cms-lab
 * @copyright       2018-2023 cms-lab
 * @link            https://cms-lab.com
 * @license         custom license: https://cms-lab.com/_documentation/meta_index/license.php
 * @license_terms   please see license
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

$MOD_META_INDEX = [
	'action'			=> "Action",
	'add_form'			=> "Add Form",
	'html_footer'		=> "HTML Footer",	
	'modify'			=> "modify",
	'preview'			=> "preview",		
	'edit'				=> "Edit",
	'copy'				=> "copy",	
	'details'			=> "Details",
	'name'				=> "Name",
	'value'				=> "Value",
	'info'				=> "Addon Info",		
	
	//	forms table	
	'form_id'		=> "Form-ID",
	'form_name'		=> "Form Name",
	'module_name'	=> "Module Name",
	'rows_count'	=> "Rows",
	'hidden_count'	=> "Hidden Fields",	

	//	rows/fields table	
	'row_no'		=> "Row",
	'field_name'	=> "Feld Name",
	'field_label'	=> "Label",
	'field_type'	=> "Feld Typ",
	'field_id'		=> "Feld_ID",
	'field_placeholder'	=> "Platzhalter",	
	'field_value'	=> "Feldwert",		

	//	tags
	'tag1'		=> "index, follow",	
	'tag2'		=> "index, nofollow",
	'tag3'		=> "noindex, follow",	
	'tag4'		=> "noindex, nofollow",

	//	messages
	'record_deleted'	=> "Datensatz wurde gelöscht",	
	'record_saved'		=> "Datensatz wurde gespeichert",
	'record_not_saved'	=> "Datensatz wurde nicht gespeichert, da doppelte Werte vorhanden sind!",	
	'record_inserted'	=> "Datensatz wurde hinzugefügt"	
];
